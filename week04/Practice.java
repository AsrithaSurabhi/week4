package week04;

public class Practice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// string examples
		// 1. create a string
		String name = "Andrew clong";
		System.out.println(name);
		
		//2. Number of characters in the string
		int numChars = name.length();
		System.out.println("Number of characters: " + numChars);
		
		//3. Get a specific character
		char charAtPosition5= name.charAt(4);
		System.out.println("character at Position 5:" + charAtPosition5);
		
		//4. Get a substring
		String sub = name.substring(0,5);
		System.out.println("Get substring 1:" + sub);
		
		String sub02 = name.substring(6);
		System.out.println("Get substring 2:" + sub02);
		
		// 5. check if one string is equal to another
		
		String a= "andrew";
		String b="Clong";
		String c= "andrew";
		 if (a.contentEquals(b)) {
			 System.out.println("a and b are same");
		 }
			 else {
				 
					System.out.println("a and b are NOT same");
			 }
		 
	
	if (a.contentEquals(c)) {
		 System.out.println("a and c are same");
	 }
		 else {
			 
				System.out.println("a and c are NOT same");
		 
	 }
		//6. Make everything uppercase
	
		String d = "yelling";
		System.out.println(d.toUpperCase());
	
	
		String p = "YeLlInnnnnGGGGG";
		System.out.println(p.toUpperCase());
		//7. Make everything lowercase
		
		String m = "yelling";
		System.out.println(m.toLowerCase());
		
		String f = "YeLlInnnnnGGGGG";
		System.out.println(f.toLowerCase());

	}
}


